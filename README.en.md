# resource_retriever

#### Description
This package retrieves data from url-format files such as http://, ftp://, package:// file://, etc., and loads the data into memory. The package:// url for ros packages is translated into a local file:// url. The resourse retriever was initially designed to load mesh files into memory, but it can be used for any type of data. The resource retriever is based on the the libcurl library.

#### Software Architecture
Software architecture description

resource_retriever has an extremely simple C++ API, consisting of two classes and one method.The resource_retriever::Retriever class is the gateway into downloading files. Its get() method returns a resource_retriever::MemoryResource which contains the file in memory.

input:
```
resource_retriever/
├── CHANGELOG.rst
├── CMakeLists.txt
├── include
│   └── resource_retriever
├── mainpage.dox
├── package.xml
├── setup.py
├── src
│   ├── resource_retriever
│   └── retriever.cpp
└── test
    ├── CMakeLists.txt
    ├── test.cpp
    ├── test.py
    └── test.txt
```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-catkin/ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-catkin/ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

output:
```
resource_retriever/
├── cmake.lock
├── env.sh
├── lib
│   ├── libresource_retriever.so
│   ├── pkgconfig
│   ├── python2.7
│   └── resource_retriever
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── resource_retriever
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
