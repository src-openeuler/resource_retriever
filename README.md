# resource_retriever

#### 介绍
此程序包从URL格式文件（例如http：//，ftp：//，package：// file：//等）中检索数据，并将数据加载到内存中。ros软件包的package：// url被转换为本地file：// url。资源检索器最初设计为将网格文件加载到内存中，但可用于任何类型的数据。资源检索器基于libcurl库。

#### 软件架构
软件架构说明
resource_retriever具有一个非常简单的C ++ API，由两个类和一个方法组成。
该resource_retriever ::Retriever是网关到下载文件。它的get（）方法返回一个resource_retriever :: MemoryResource，它包含内存中的文件。

文件内容:
```
resource_retriever/
├── CHANGELOG.rst
├── CMakeLists.txt
├── include
│   └── resource_retriever
├── mainpage.dox
├── package.xml
├── setup.py
├── src
│   ├── resource_retriever
│   └── retriever.cpp
└── test
    ├── CMakeLists.txt
    ├── test.cpp
    ├── test.py
    └── test.txt
```

#### 安装教程

1. 下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-catkin/ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-catkin/ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.x86_64.rpm 

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-resource_retriever-1.12.6-0.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
resource_retriever/
├── cmake.lock
├── env.sh
├── lib
│   ├── libresource_retriever.so
│   ├── pkgconfig
│   ├── python2.7
│   └── resource_retriever
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── resource_retriever
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
